#include <stdio.h>
#include <stdlib.h>
#include "minako.h"

yystype yylval;
int token;

void program();
void functionDefinition();
void functionCall();
void statementList();
void block();
void statement();
void ifStatement();
void returnStatement();
void printF();
void type();
void statAssignment();
void assignment();
void expr();
void simpexpr();
void term();
void factor();

void lex()
{
    if (token == EOF)
        return;
    token = yylex();
}

int checkTerminal(int tk)
{
    if (token != tk)
    {
        printf("Line: %3d\t", yylineno);
        printf("Error, got \"%c\" but \"%c\" is expected \n", token, tk);
        exit(-1);
    }
    lex();
    return 1;
}
// Datentyp check
void type()
{
    if (token == KW_BOOLEAN)
    {
        checkTerminal(KW_BOOLEAN);
        return;
    }
    else if (token == KW_FLOAT)
    {
        checkTerminal(KW_FLOAT);
        return;
    }
    else if (token == KW_INT)
    {
        checkTerminal(KW_INT);
        return;
    }
    else if (token == KW_VOID)
    {
        checkTerminal(KW_VOID);
        return;
    }
    printf("Line: %3d\t", yylineno);
    printf("Got %c needed TYPE here", token);
    exit(-1);
}

void factor()
{

    if (token == CONST_INT)
    {
        checkTerminal(CONST_INT);
        return;
    }
    else if (token == CONST_FLOAT)
    {
        checkTerminal(CONST_FLOAT);
        return;
    }
    else if (token == CONST_BOOLEAN)
    {
        checkTerminal(CONST_BOOLEAN);
        return;
    }
    else if (token == '(')
    {
        checkTerminal('(');
        assignment();
        checkTerminal(')');
        return;
    }
    else if (token == ID)
    {
        checkTerminal(ID);
        if (token == '(')
        {
            functionCall();
        }
        return;
    }
    printf("Line: %3d\t", yylineno);
    printf("got %c but need FACTOR here", token);
    exit(-1);
}

void term()
{

    factor();
    while (token == EQ || token == '*' || token == '/')
    {
        if (token == EQ)
        {
            checkTerminal(EQ);
            factor();
            continue;
        }
        else if (token == '/')
        {
            checkTerminal('/');
            factor();
            continue;
        }
        else if (token == '*')
        {
            checkTerminal('*');
            factor();
            continue;
        }
        printf("Line: %3d\t", yylineno);
        printf("got %c but need TERM here", token);
        exit(-1);
    }
}
// Addition Sub und oder
void simpexpr()
{

    if (token == '-')
    {
        checkTerminal('-');
    }
    term();
    while (token == OR || token == '-' || token == '+')
    {
        if (token == OR)
        {
            checkTerminal(OR);
            term();
            continue;
        }
        else if (token == '-')
        {
            checkTerminal('-');
            term();
            continue;
        }
        else if (token == '+')
        {
            checkTerminal('+');
            term();
            continue;
        }
    }
}

// Expression check
void expr()
{
    simpexpr();
    if (token == EQ)
    {
        checkTerminal(EQ);
        simpexpr();
        return;
    }
    else if (token == NEQ)
    {
        checkTerminal(NEQ);
        simpexpr();
        return;
    }
    else if (token == LEQ)
    {
        checkTerminal(LEQ);
        simpexpr();
        return;
    }
    else if (token == GEQ)
    {
        checkTerminal(GEQ);
        simpexpr();
        return;
    }
    else if (token == GRT)
    {
        checkTerminal(GRT);
        simpexpr();
        return;
    }
    else if (token == LSS)
    {
        checkTerminal(LSS);
        simpexpr();
        return;
    }
}

//Zuordnung / Compare 
void assignment()
{

    if (token == '(')
    {
        checkTerminal('(');
        checkTerminal(ID);
        checkTerminal('=');
        assignment();
        checkTerminal(')');
    }
    else
    {
        expr();
    }
}
// if clause
void ifStatement()
{

    checkTerminal(KW_IF);
    checkTerminal('(');
    assignment();
    checkTerminal(')');
    block();
}
void returnStatement()
{

    checkTerminal(KW_RETURN);
    if (token != ';')
    {
        assignment();
    }
}
void printF()
{

    checkTerminal(KW_PRINTF);
    checkTerminal('(');
    assignment();
    checkTerminal(')');
}
void statAssignment()
{

    checkTerminal('=');
    assignment();
}
void functionCall()
{

    checkTerminal('(');
    if (token != ')')
    {
        assignment();
        while (token == ',')
        {
            checkTerminal(',');
            assignment();
        }
        checkTerminal(')');
    }
    else
    {
        checkTerminal(')');
    }
}

// Statement check
void statement()
{

    if (token == KW_IF)
    {
        ifStatement();
        return;
    }
    else if (token == KW_RETURN)
    {
        returnStatement();
        checkTerminal(';');
        return;
    }
    else if (token == KW_PRINTF)
    {
        printF();
        checkTerminal(';');
        return;
    }
    else if (token == ID)
    {
        checkTerminal(ID);
        if (token == '=')
        {
            statAssignment();
        }
        else
        {
            functionCall();
        }
        checkTerminal(';');
        return;
    }
    printf("got %c but need statement here", token);
    exit(-1);
}
// Anfang Ende Check
void block()
{

    if (token == '{')
    {
        if (token == '{')
        {
            checkTerminal('{');
        }
     

        statementList();

        if (token == '}')
        {
            checkTerminal('}');
        }

        return;
    }
    else
    {
        statement();
        return;
    }
}

void statementList()
{

    while (token != '}')
    {
        block();
    }
}


void functionDefinition()
{

     type();
    checkTerminal(ID);
    checkTerminal('(');
    if (token != ')')
    {
        checkTerminal(ID);
        while (token == ',')
        {
            checkTerminal(',');
            checkTerminal(ID);
        }
    }
    checkTerminal(')');
    if (token == '{')
    {
        checkTerminal('{');
    }

    statementList();
    if (token == '}')
    {
        checkTerminal('}');
    }

}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        yyin = stdin;
    }
    else
    {
        yyin = fopen(argv[1], "r");
        if (yyin == 0)
        {
            fprintf(stderr, "Fehler: Konnte Datei %s nicht zum lesen oeffnen.\n", argv[1]);
            exit(-1);
        }
    }
    lex();
    while (token != EOF)
    {
        // lex();
       functionDefinition();
    }

    return 0;
}
